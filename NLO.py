import pygame
import PIL
from PIL import Image
from MetakEnemy import MetakEnemy

class NLO(pygame.sprite.Sprite):

	def __init__(self, x, y, right, enemies, all_sprite_list, mainship, myBullets):
		pygame.sprite.Sprite.__init__(self)

		self.image = pygame.image.load('Sprites/Brod2.png').convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.bottom = y
		self.rect.right = x
		
		self.right = right
		self.mainship = mainship
		
		self.bullets = myBullets
		
		self.enemies = enemies
		self.cnt = 0
		self.energy = 7
		self.points = 200
		self.shootTime = 15
		self.z = 0
		
		self.killme = False
		self.killmecnt = 10
		
		self.all_sprite_list = all_sprite_list
		
	def animate(self):
	
		if(self.right):
			self.rect.x -= 5
		else:
			self.rect.x += 5
		
		if(self.killme):
		
			self.cnt += 1
			w,h = pygame.display.get_surface().get_size()
			
			if(self.rect.left > w):
				self.kl()
			
			if(self.cnt < 59):
				self.im.seek(self.cnt)
				self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
			else:
				self.cnt = 0
				self.im.seek(0)
				self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
				
			if(self.killme):
				if(self.killmecnt>0):
					self.killmecnt -= 1
				else:
					self.kl()
		#Pucanje
		if(not self.killme):
			self.shoot()
	
	def kl(self):
		self.kill()
		self.enemies.remove(self)
		self.all_sprite_list.update()
			
	def destroyme(self):
		self.im = Image.open('Sprites/ExplosionBrod.gif')
		self.killme = True
		self.mainship.add_points(self.points)
		
	def shoot(self):
		self.shootTime -=1
		if(self.shootTime == 0):
			m = MetakEnemy(self.rect.centerx,self.rect.bottom,self.mainship,self.bullets)
			self.bullets.append(m)
			self.all_sprite_list.add(m)
			self.shootTime = 50
			
