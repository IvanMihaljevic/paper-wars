import pygame
import PIL
from PIL import Image

class MetakEnemy(pygame.sprite.Sprite):

	def __init__(self,x,y, mainship, allOfUs):
		
		pygame.sprite.Sprite.__init__(self)

		self.mainship = mainship
		self.allOfUs = allOfUs
		
		self.im = Image.open('Sprites/NepMetak.png')
		self.image = pygame.image.load('Sprites/NepMetak.png').convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.top = y
		self.rect.centerx = x
		self.z = 0
		
	def is_collided_with(self, Sprite):
		return self.rect.colliderect(Sprite.rect)
		
	def animate(self):
		self.rect.y += 7
		
		if(self.rect.bottom < 0):
			self.killMe()
			
		#Collision detection
		if(self.is_collided_with(self.mainship) and not self.mainship.beeingHit == 0):
			self.kill()
			try:
				self.allOfUs.remove(self)
			except:
				dummy = 1
			#e.energy -= 1
			#if(e.energy <= 0):
			#	e.destroyme()
			
	def killMe(self):
		self.kill()
		try:
			self.allOfUs.remove(self)
		except:
			dummy = 1
		
