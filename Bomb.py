import pygame
import PIL
from PIL import Image

class Bomb(pygame.sprite.Sprite):

	def __init__(self,x,y,enemies,all_sprite_list,mainship):
		pygame.sprite.Sprite.__init__(self)

		self.im = Image.open('Sprites/bomb-kon.gif')
		self.cnt = 0
		self.energy = 2
		self.points = 50
		self.image = pygame.image.load('Sprites/Bomb-kon.gif').convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.centery = y
		self.rect.centerx = x
		self.ispy = y
		self.ispx = x
		self.all_sprite_list = all_sprite_list
		self.z = 1100
		
		self.sz = (0,0)
		self.image = pygame.transform.scale(self.image, self.sz)
		
		w,h = pygame.display.get_surface().get_size()
		self.ociste = (w/2+30,h/2+50,-200)
		
		self.mainship = mainship
		
		self.killme = False
		self.killmecnt = 10
		
		self.enemies = enemies
		
	def animate(self):
	
		self.cnt += 1
		
		self.z -= 30
		if(self.ispx < self.ociste[0]):
			self.rect.x -= 10
		elif(self.ispx > self.ociste[0]):
			self.rect.x += 10
		self.projection()
		
		w,h = pygame.display.get_surface().get_size()
		
		if(self.rect.top > h or self.rect.right < 0 or self.rect.left > w or self.z < self.ociste[2]):
			self.kl()
		
		if(self.cnt < 31):
			self.im.seek(self.cnt)
			self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
			self.image = pygame.transform.scale(self.image, self.sz)
		else:
			self.cnt = 0
			self.im.seek(0)
			self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
			self.image = pygame.transform.scale(self.image, self.sz)
			
		if(self.killme):
			if(self.killmecnt>0):
				self.killmecnt -= 1
			else:
				self.kl()
	
	def kl(self):
		self.kill()
		try:
			self.enemies.remove(self)
			self.all_sprite_list.update()
		except:
			dummy = 1
			
	def destroyme(self):
		self.im = Image.open('Sprites/Explosion.gif')
		self.cnt = 0
		self.killme = True
		self.mainship.add_points(self.points)
		
	def projection(self):
		ociste = [self.ociste[0],self.ociste[1],self.ociste[2]]
			
		if(self.ispx < ociste[0]):
			ociste[0] -= 50*(float(ociste[0]-self.ispx)/ociste[0])
		elif(self.ispx > ociste[0]):
			ociste[0] += 50*(float(self.ispx-ociste[0])/ociste[0])	
		
		koef = 0
		try:
			koef = float(-ociste[2])/(self.z-ociste[2])
		except:
			self.kill()
		xnew = koef*(self.rect.centerx-ociste[0]) + ociste[0]
		ynew = koef*(self.rect.centery-ociste[1]) + ociste[1]
		
		self.rect.centerx = xnew
		self.rect.centery = ynew
		
		x1 = koef*(self.rect.left-ociste[0]) + ociste[0]
		x2 = koef*(self.rect.right-ociste[0]) + ociste[0]
		y1 = koef*(self.rect.top-ociste[1]) + ociste[1]
		y2 = koef*(self.rect.bottom-ociste[1]) + ociste[1]
		
		self.sz = (int(abs(x2 - x1)), int(abs(y2 -y1)))
			