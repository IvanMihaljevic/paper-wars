import pygame
import PIL
from PIL import Image

class Metak(pygame.sprite.Sprite):

	def __init__(self,x,y, enemies, enemyBullets, allOfUs, game2):
		
		pygame.sprite.Sprite.__init__(self)

		self.enemies = enemies
		self.enemyBullets = enemyBullets
		self.allOfUs = allOfUs
		
		self.im = Image.open('Sprites/Metak.gif')
		self.image = pygame.image.load('Sprites/Metak.gif').convert_alpha()
		self.cnt = 0
		self.rect = self.image.get_rect()
		self.rect.bottom = y
		self.rect.centerx = x
		
		self.ispx = x
		self.ispy = y
		
		self.z = 0
		
		w, h = pygame.display.get_surface().get_size()
		self.ociste = (w/2+22,h/2+50,-200)
		
		self.size = self.rect.size
		
		self.game2 = game2
		
	def is_collided_with(self, Sprite, z = 0):
		if(not self.game2):
			return self.rect.colliderect(Sprite.rect)
		else:
			if(self.rect.colliderect(Sprite.rect) and abs(self.z - z) <= 80 ):
				return True
			else:
				return False
		
	def animate(self):
		
		if(not self.game2):
			self.rect.y -= 8
		else:
			self.z += 30
			self.projection()
		
		if(self.rect.bottom < 0 or self.z > 800):
			self.kill()
			try:
				self.allOfUs.remove(self)
			except:
				dummy = 1
		
		self.cnt += 1
		
		if(self.cnt < 30):
			self.im.seek(self.im.tell() + 1)
			self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
			if(self.game2):
				self.image = pygame.transform.scale(self.image, self.sz)
		else:
			self.cnt = 0
			self.im.seek(0)
			self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
			
		#Collision detection
		for e in self.enemies:
			if(self.is_collided_with(e,e.z) and not e.killme):
				self.kill()
				try:
					self.allOfUs.remove(self)
				except:
					dummy = 1
				e.energy -= 1
				if(e.energy <= 0):
					e.destroyme()
					
		for e in self.enemyBullets:
			if(self.is_collided_with(e)):
				self.kill()
				try:
					self.allOfUs.remove(self)
				except:
					dummy = 1
				e.killMe()
			
	def projection(self):
		ociste = [self.ociste[0],self.ociste[1],self.ociste[2]]
		
		if(self.ispx < ociste[0]):
			ociste[0] -= 50*(float(ociste[0]-self.ispx)/ociste[0])
		elif(self.ispx > ociste[0]):
			ociste[0] += 50*(float(self.ispx-ociste[0])/ociste[0])
			
		koef = float(-ociste[2])/(self.z-ociste[2])
		xnew = koef*(self.rect.centerx-ociste[0]) + ociste[0]
		ynew = koef*(self.rect.centery-ociste[1]) + ociste[1]
		
		self.rect.centerx = xnew
		self.rect.centery = ynew
		
		x1 = koef*(self.rect.left-ociste[0]) + ociste[0]
		x2 = koef*(self.rect.right-ociste[0]) + ociste[0]
		y1 = koef*(self.rect.top-ociste[1]) + ociste[1]
		y2 = koef*(self.rect.bottom-ociste[1]) + ociste[1]
		
		self.sz = (int(x2 - x1), int(y2 -y1))
