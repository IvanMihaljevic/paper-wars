import io
import pygame
from Ship import ship
from NLO import NLO
import PIL
from PIL import Image
from Meteor import Meteor
from Bomb import Bomb

class Games(object):

	def game1(all_sprites_list,enemies,time,mainShip,poz,enemyBullets):
		#Animacija
		mainShip.animate()
		#Upravljanje
		mainShip.handleKeys()
		#Meci
		mainShip.handleBullets()
		
		#Meteori
		w,h = pygame.display.get_surface().get_size()
		
		pozMet = [0, w/2 - 150, w/2 - 800, w/2 + 600, 0, w/2 - 400, w/2 + 200, w/2, w/2 - 200, w/2 + 100, w/2 - 100]
		if(time == 10 or time == 73 or time == 79 or time == 114 or time == 130 or time == 170 or time == 220 or (time>220 and time%50 == 0 and time <500) or (time> 1000 and time%50 == 0 and time <3000)):
			met = Meteor(pozMet[poz],0,enemies,all_sprites_list,mainShip)
			enemies.append(met)
			all_sprites_list.add(met)
			poz += 1
			if(poz >= len(pozMet)):
				poz = 0
			
		#Brodeki
		pozBr = [170,200,250,350,250,400,170,300,200,300,170,250]
		desn = [False,False,True,False,True,True,False,False,True,False,True,True]
		if(time == 200 or (time>400 and time%50 == 0 and time <1000) or time ==1500 or time == 2000 or time == 2200 or time == 2500 or time == 2800 or (time> 3000 and time%50 == 0 and time <6000)):
			px = 0
			if(desn[poz]):
				px = w + 250
			met = NLO(px,pozBr[poz],desn[poz],enemies,all_sprites_list,mainShip,enemyBullets)
			enemies.append(met)
			all_sprites_list.add(met)
			poz += 1
			if(poz >= len(pozBr)):
				poz = 0
		
		#Pomicanje neprijatelja
		for e in enemies:
			e.animate()
			
		#Pomicanje metaka
		for b in enemyBullets:
			b.animate()
		
		return poz
		
	def game2(back2,screen,mainShip,poz,time,all_sprites_list,enemies):
		im = pygame.transform.scale(pygame.image.fromstring(back2.convert("RGBA").tobytes(), back2.size, "RGBA"),pygame.display.get_surface().get_size())
		screen.blit(im,(0,0))
		#Animacija
		mainShip.animate()
		#Upravljanje
		mainShip.handleKeys()
		#Meci
		mainShip.handleBullets()
		
		#Bombe
		pozB = [0,1,2,1,0,1,2,2,1,0,1,2,2,1,2,0,1]
		if(time%50 == 0 and (time < 500 or time > 550)):
			
			w, h = pygame.display.get_surface().get_size()
			
			x = w/2 + 30
			y = h/2 + 50
			if(pozB[poz] == 1):
				x -= 100
				
			elif(pozB[poz] == 2):
				x += 100
				
			met = Bomb(x,y,enemies,all_sprites_list,mainShip)
			
			enemies.append(met)
			all_sprites_list.add(met)
			poz += 1
			if(poz >= len(pozB)):
				poz = 0
		
		
		#Pomicanje neprijatelja
		for e in enemies:
			e.animate()
		
		return poz
		
	def renderLivesAndEnergy(screen, mainShip):
		pygame.draw.polygon(screen,pygame.Color(50,50,0,100),[(890,0),(1200,0),(1200,130),(890,130)])
		myfont = pygame.font.SysFont("Bauhaus 93", 58)
		label = myfont.render("LIVES:" + str(mainShip.lives - 1), 1, (120,120,120))
		screen.blit(label, (920, 10))
		label = myfont.render("ENERGY:" + str(mainShip.energy), 1, (120,120,120))
		screen.blit(label, (920, 60))
		#pygame.draw.polygon(screen,pygame.Color(50,50,0,100),[(0,0),(280,0),(280,80),(0,80)])
		label = myfont.render(str(mainShip.points), 1, (120,120,120))
		screen.blit(label, (20, 10))