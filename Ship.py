import pygame
from Metak import Metak
from Meteor import Meteor
import PIL
from PIL import Image

class ship(pygame.sprite.Sprite):

	def __init__(self,x,y, all_sprite_list, enemies, enemyBullets):
		pygame.sprite.Sprite.__init__(self)

		self.all_sprite_list = all_sprite_list
		self.enemies = enemies
		self.enemyBullets = enemyBullets
		self.bullets = []
		#Vrijeme do novog pucnja
		self.fireCount = 0
		
		self.beeingHit = 0
		self.energy = 5
		self.lives = 4
		self.points = 0
		self.secondGame = False
		
		self.gotExtraLife = True
		
		self.im = Image.open('Sprites/Letjelica4.gif')
		self.imc = Image.open('Sprites/Letjelica4.gif')
		self.iml = Image.open('Sprites/Letjelica-Lijevo.gif')
		self.imr = Image.open('Sprites/Letjelica-Desno.gif')
		
		self.imcost = Image.open('Sprites/Letjelica-ost.gif')
		self.imlost = Image.open('Sprites/Letjelica-Lijevo-ost.gif')
		self.imrost = Image.open('Sprites/Letjelica-Desno-ost.gif')
		
		self.immrtv = Image.open('Sprites/LetjelicaMrtva.gif')
		
		self.cnt = 0
		self.image = pygame.image.load('Sprites/Letjelica4.png').convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.bottom = y
		self.rect.centerx = x
		
	def animate(self):
		
		if(self.beeingHit > 0):
			self.beeingHit -= 1
			if(self.beeingHit == 0 and self.energy == 0):
				self.energy = 5
				self.lives -= 1
				self.beeingHit = 80
		
		self.cnt += 1
		
		key = pygame.key.get_pressed()
		
		if(self.energy>0):
			if key[pygame.K_RIGHT]:
				if(self.beeingHit > 0):
					self.im = self.imrost
				else:
					self.im = self.imr
				
			elif key[pygame.K_LEFT]:
				if(self.beeingHit > 0):
					self.im = self.imlost
				else:
					self.im = self.iml
			
			else:
				if(self.beeingHit > 0):
					self.im = self.imcost
				else:
					self.im = self.imc
		
		if(self.cnt < 61):
			self.im.seek(self.cnt)
			self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
		else:
			if(self.energy == 0):
				self.im = Image.open("Sprites/Prazno.gif")
			self.cnt = 0
			self.im.seek(0)
			self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
	
		#Kolizija
		for e in self.enemies:
			if(self.is_collided_with(e) and self.beeingHit == 0):
				if(not self.secondGame or (self.secondGame and e.z < 50)):
					self.beeingHit = 80
					self.energy -= 1
					if(self.energy == 0):
						self.im = self.immrtv
						self.beeingHit = 143
						self.cnt = 0
				
		for e in self.enemyBullets:
			if(self.is_collided_with(e) and self.beeingHit == 0):
				self.beeingHit = 80
				self.energy -= 1
				if(self.energy == 0):
					self.im = self.immrtv
					self.beeingHit = 143
					self.cnt = 0
			
			
	def handleKeys(self):
		key = pygame.key.get_pressed()
		
		w,h = pygame.display.get_surface().get_size()
		
		if(self.energy > 0):
			if key[pygame.K_DOWN] and self.rect.bottom < h-7 and not self.secondGame:
				self.rect.y += 7
			if key[pygame.K_UP] and self.rect.y > 7 and not self.secondGame:
				self.rect.y -= 7
			if key[pygame.K_RIGHT] and self.rect.right < w-7:
				self.rect.x += 7
			if key[pygame.K_LEFT] and self.rect.x > 7:
				self.rect.x -= 7
			if key[pygame.K_SPACE]:
				self.shoot()
				
	def handleBullets(self):
		if(self.fireCount > 0):
			self.fireCount -= 1
		for b in self.bullets:
			b.animate()
			
	def is_collided_with(self, Sprite):
		return self.rect.colliderect(Sprite.rect)
		
	def add_points(self, ammount):
		self.points += ammount
		if(self.points % 13000 >= 0 and self.points % 13000 <= 150):
			if(not self.gotExtraLife):
				self.lives += 1
				self.gotExtraLife = True
		else:
			self.gotExtraLife = False

	def enter_second_game(self):
		self.im = Image.open('Sprites/Letjelica-druga.gif')
		self.imc = Image.open('Sprites/Letjelica-druga.gif')
		self.iml = Image.open('Sprites/Letjelica-Lijevo-druga.gif')
		self.imr = Image.open('Sprites/Letjelica-Desno-druga.gif')
		
		self.imcost = Image.open('Sprites/Letjelica-druga-ost.gif')
		self.imlost = Image.open('Sprites/Letjelica-Lijevo-druga-ost.gif')
		self.imrost = Image.open('Sprites/Letjelica-Desno-druga-ost.gif')
		
		self.immrtv = Image.open('Sprites/LetjelicaMrtva-druga.gif')
		
		self.rect.centerx = 600
		self.rect.bottom = 700
		self.image = pygame.image.load('Sprites/Letjelica-druga.png').convert_alpha()
		self.secondGame = True
			
	def shoot(self):
		if(self.fireCount == 0 and len(self.bullets)<=5):
			m = Metak(self.rect.centerx,self.rect.y,self.enemies,self.enemyBullets,self.bullets,self.secondGame)
			self.bullets.append(m)
			self.all_sprite_list.add(m)
			self.fireCount = 5
		
