import io
import pygame
from Ship import ship
from NLO import NLO
import PIL
from PIL import Image
from MainGame import Games
from Metak import Metak
from Meteor import Meteor

pygame.init()
size = (1200, 752)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Paper wars")

all_sprites_list = pygame.sprite.Group()
enemies = []
#Meci neprijatelja ; Oni su zasebna kategorija
enemyBullets = []
tm = -80
poz = 0
gameOver = 80
mainShip = ship(600,700,all_sprites_list, enemies, enemyBullets)
all_sprites_list.add(mainShip)
game = 0
curgame = game
cnt = 0
back2 = Image.open("Backgrounds/Star-kon4.gif")

carryOn = True
clock = pygame.time.Clock()
begAnim = Image.open("Backgrounds/LogoAnim.gif")
begAnim.convert("RGBA")
levelFinAnim = Image.open("Backgrounds/InsertIntoHole.gif")

while carryOn:
	for event in pygame.event.get(): # User did something
		if event.type == pygame.QUIT: # If user clicked close
			carryOn = False # Flag that we are done so we exit this loop
	
	all_sprites_list.update()
	
	screen.fill((0,0,0))
	im = pygame.transform.scale(pygame.image.load("Backgrounds/Space.png"),pygame.display.get_surface().get_size())
	screen.blit(im,(0,0))
	
	if(game == 0):
		
		im = pygame.image.fromstring(begAnim.convert("RGBA").tobytes(), begAnim.size, "RGBA")
		im = pygame.transform.scale(im, pygame.display.get_surface().get_size())
		screen.blit(im,(0,0))
		
		try:
			begAnim.seek(begAnim.tell() + 1)
		except:
			game = 1
    
	if(game == 1):
	
		#Init
		all_sprites_list = pygame.sprite.Group()
		enemies = []
		tm = -80
		cnt = 0
		poz = 0
		mainShip = ship(600,700,all_sprites_list, enemies, enemyBullets)
		all_sprites_list.add(mainShip)
		back2 = Image.open("Backgrounds/Star-kon4.gif")

		im = pygame.transform.scale(pygame.image.load("Backgrounds/Menu.png"),pygame.display.get_surface().get_size())
		screen.blit(im,(0,0))
		p0,p1 = pygame.mouse.get_pos()
		b0,b1,b2 = pygame.mouse.get_pressed()
		k1 = pygame.display.get_surface().get_size()[0]
		k2 = pygame.display.get_surface().get_size()[1]
		d1 = float(p0)/k1
		d2 = float(p1)/k2
		if(d1 > 0.37 and d1 < 0.561 and d2 > 0.33 and d2 < 0.467):
			im = pygame.transform.scale(pygame.image.load("Backgrounds/Menu-b1.png"),pygame.display.get_surface().get_size())
			screen.blit(im,(0,0))
			if(b0):
				game = 2
				curgame = 2
		elif(d1 > 0.37 and d1 < 0.561 and d2 > 0.51 and d2 < 0.647):
			im = pygame.transform.scale(pygame.image.load("Backgrounds/Menu-b2.png"),pygame.display.get_surface().get_size())
			screen.blit(im,(0,0))
			if(b0):
				game = 7
		elif(d1 > 0.37 and d1 < 0.561 and d2 > 0.703 and d2 < 0.837):
			im = pygame.transform.scale(pygame.image.load("Backgrounds/Menu-b3.png"),pygame.display.get_surface().get_size())
			screen.blit(im,(0,0))
			if(b0):
				break
	
	elif(game == 2):
		
		all_sprites_list.update()
		all_sprites_list.draw(screen)
		
		#Energija i zivoti
		Games.renderLivesAndEnergy(screen, mainShip)
		
		poz = Games.game1(all_sprites_list, enemies, tm, mainShip, poz, enemyBullets)
		tm += 1
		key = pygame.key.get_pressed()
		
		if key[pygame.K_ESCAPE]:
			game = 3
		
		if(mainShip.lives == 0):
			game = 10
			
		if(tm >= 6300 and mainShip.energy > 0):
			game = 4
			curgame = 4
			tm = 1
			mainShip.enter_second_game()
			
			
	elif(game == 3):
		im = pygame.transform.scale(pygame.image.load("Backgrounds/Choice.png"),pygame.display.get_surface().get_size())
		screen.blit(im,(0,0))
		
		key = pygame.key.get_pressed()
		
		if key[pygame.K_y]:
			game = 1
			
		elif key[pygame.K_n]:
			game = curgame
			
	elif(game == 4):
	
		im = pygame.image.fromstring(begAnim.convert("RGBA").tobytes(), begAnim.size, "RGBA")
		im = pygame.transform.scale(im, pygame.display.get_surface().get_size())
		screen.blit(im,(0,0))

		back2.seek(cnt)
		poz = Games.game2(back2,screen,mainShip,poz,tm,all_sprites_list,enemies)
		
		if(tm<500):
			cnt += 2			
			if(cnt > 15):
				cnt = 0
		else:
			myfont = pygame.font.SysFont("Bauhaus 93", 58)
			label = myfont.render("PRESS RETURN NOW!!!", 1, (200,10,10))
			w,h = pygame.display.get_surface().get_size()
			screen.blit(label, (w/2-300, 50))
			cnt+=1			
			if(cnt > 49):
				cnt = 0
		
		all_sprites_list.update()
		all_sprites_list.draw(screen)
		
		key = pygame.key.get_pressed()
		
		if key[pygame.K_ESCAPE]:
			game = 3
		
		if(mainShip.lives == 0):
			game = 10
			
		#Energija i zivoti
		Games.renderLivesAndEnergy(screen, mainShip)
		tm += 1
		
		if(tm == 500):
			cnt = 0
			back2 = Image.open("Backgrounds/StarWithHole.gif")
			
		if(tm >= 500 and tm <= 550):
			key = pygame.key.get_pressed()
		
			if key[pygame.K_RETURN]:
				game = 5
				cnt = 0
		
		if(tm>550):
			tm = 1
			back2 = Image.open("Backgrounds/Star-kon4.gif")
			
	elif(game == 5):
		#TODO: Gif unistenja paper star-a
		gameOver = 160
		
		levelFinAnim.seek(cnt)
		
		im = pygame.image.fromstring(levelFinAnim.convert("RGBA").tobytes(), levelFinAnim.size, "RGBA")
		im = pygame.transform.scale(im, pygame.display.get_surface().get_size())
		screen.blit(im,(0,0))
		
		if(cnt<10):
			cnt+=1
		else:
			cnt+=2
		if(cnt>99):
			levelFinAnim.seek(0)
			cnt = 0	
			game = 6
		
	elif(game == 6):
		im = pygame.transform.scale(pygame.image.load("Backgrounds/Win.png"),pygame.display.get_surface().get_size())
		screen.blit(im,(0,0))
		gameOver -= 1
		if(gameOver == 0):
			gameOver = 80
			game = 1
			
	elif(game == 7):
		im = pygame.transform.scale(pygame.image.load("Backgrounds/About.png"),pygame.display.get_surface().get_size())
		screen.blit(im,(0,0))
		
		p0,p1 = pygame.mouse.get_pos()
		b0,b1,b2 = pygame.mouse.get_pressed()
		k1 = pygame.display.get_surface().get_size()[0]
		k2 = pygame.display.get_surface().get_size()[1]
		d1 = float(p0)/k1
		d2 = float(p1)/k2
		if(d1 > 0.38625 and d1 < 0.55625 and d2 > 0.904 and d2 < 0.988):
			im = pygame.transform.scale(pygame.image.load("Backgrounds/About-b1.png"),pygame.display.get_surface().get_size())
			screen.blit(im,(0,0))
			if(b0):
				game = 1
			
	elif(game == 10):
		im = pygame.transform.scale(pygame.image.load("Backgrounds/GameOver.png"),pygame.display.get_surface().get_size())
		screen.blit(im,(0,0))
		gameOver -= 1
		if(gameOver == 0):
			gameOver = 80
			game = 1

	pygame.display.flip()

	clock.tick(60)

#Once we have exited the main program loop we can stop the game engine:
pygame.quit()
